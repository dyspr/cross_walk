var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var maxSteps = 6
var steps = 1
var initSize = 0.09
var dimension = 9
var array = create2DArray(dimension, dimension, 0, false)

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize)
      fill(255)
      noStroke()
      drawTexturedTile(array[i][j][0], array[i][j][1], boardSize * initSize + 1)
      pop()

    }
  }

  steps = Math.floor(Math.random() * maxSteps)
  array[Math.floor(Math.random() * dimension)][Math.floor(Math.random() * dimension)] = [Math.floor(Math.random() * 2), Math.floor(Math.random() * steps) * 2 + 1]
}

function drawTexturedTile(orientation, num, size) {
  if (orientation === 0) {
    for (var i = 0; i < num; i++) {
      push()
      translate(0, (i - Math.floor(num * 0.5)) * size / (num))
      rect(0, 0, size, size / (num * 2))
      pop()
    }
  } else {
    for (var i = 0; i < num; i++) {
      push()
      translate((i - Math.floor(num * 0.5)) * size / (num), 0)
      rect(0, 0, size / (num * 2), size)
      pop()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = [Math.floor(Math.random() * 2), Math.floor(Math.random() * maxSteps) * 2 + 1]
      }
    }
    array[i] = columns
  }
  return array
}
